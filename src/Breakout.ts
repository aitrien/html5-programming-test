export class Breakout extends Phaser.Game {
	constructor() {
		super(600, 900, Phaser.AUTO, 'content', State);
	}
}

class State extends Phaser.State {
	preload() {
		this.load.image('paddle', '../src/assets/paddle.png');
		this.load.image('ball', '../src/assets/ball.png');
		this.load.image('brick_1', '../src/assets/brick1.png');
		this.load.image('brick_2', '../src/assets/brick2.png');
		this.load.image('brick_3', '../src/assets/brick3.png');
	}

	paddle: Phaser.Sprite;
	ball: Phaser.Sprite;
	velocity: number = 400;
	bricks: Phaser.Group;
	ballInPlay: boolean = false;
	score: number = 0;
	combo: number = 0;
	lives: number = 3;
	scoreText: Phaser.Text;
	comboText: Phaser.Text;
	livesText: Phaser.Text;
	tipText: Phaser.Text;

	create() {
		this.physics.startSystem(Phaser.Physics.ARCADE);
		this.physics.arcade.checkCollision.down = false;
		this.stage.backgroundColor = '#FFFFBA';
		this.fixScreen();
		
		this.paddle = this.add.sprite(300, 800, 'paddle');
		this.paddle.anchor.setTo(0.5, 0.5);
		this.physics.enable(this.paddle, Phaser.Physics.ARCADE);
		this.paddle.body.collideWorldBounds = true;
		this.paddle.body.immovable = true;

		this.ball = this.add.sprite(300, 770, 'ball');
		this.ball.anchor.set(0.5, 0.5);
		this.physics.enable(this.ball, Phaser.Physics.ARCADE);
		this.ball.body.collideWorldBounds = true;
		this.ball.checkWorldBounds = true;
		this.ball.body.bounce.set(1);

		this.bricks = this.add.group();
		this.bricks.physicsBodyType = Phaser.Physics.ARCADE;
		this.bricks.enableBody = true;

		// Generate all the brick sprites
		let brick: Phaser.Sprite;
		for (var y = 0; y < 6; y++) {
			for (var x = 0; x < 10; x++) {
				brick = this.bricks.create(50 + (x * 50), 100 + (y * 60), 'brick_' + Math.ceil(y / 2 + 0.5));
				brick.body.bounce.set(1);
				brick.body.immovable = true;
			}
		}

		this.scoreText = this.add.text(50, 40, 'Score 0', { font: "30px Consolas", fill: "#000000", align: "left" });
		this.comboText = this.add.text(200, 500, 'Combo 0', { font: "50px Consolas", fill: "#000000", align: "left" });
		this.comboText.visible = false;
		this.livesText = this.add.text(460, 40, 'Lives 3', { font: "30px Consolas", fill: "#000000", align: "left" });
		this.tipText = this.add.text(200, 550, 'Tap to aim ball', { font: "30px Consolas", fill: "#000000", align: "left" });

		this.input.onDown.add(this.serveBall, this);
		this.ball.events.onOutOfBounds.add(this.ballLost, this);
	}

	update() {
		if (this.ballInPlay) {
			this.paddle.x = this.input.x;
			this.physics.arcade.collide(this.ball, this.paddle, this.ballHitsPaddle, null, this);
			this.physics.arcade.collide(this.ball, this.bricks, this.ballHitsBrick, null, this);
		}
		else {
			// Paddle set to the middle when serving
			this.paddle.x = 300;
		}

		// Correct paddle position if moved off-screen
		if (this.paddle.x < 35) {
			this.paddle.x = 35;
		}
		else if (this.paddle.x > 600 - 35) {
			this.paddle.x = 600 -35;
		}
	}

	// Stretches screen to match the current orientation/resolution
	fixScreen() {
		this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.scale.minWidth = window.innerWidth;
		this.scale.maxWidth = window.innerWidth;
		this.scale.minHeight = window.innerHeight;
		this.scale.maxHeight = window.innerHeight;
		this.scale.refresh();
	}

	// Serves/launches the ball in the direction of the user input
	serveBall() {
		if (!this.ballInPlay) {
			this.tipText.visible = false;
			this.ballInPlay = true;
			// limit initial angle of the ball to area of the bricks 
			let angle:number = Math.min(Math.max(this.physics.arcade.angleToPointer(this.ball), -2.3), -0.7);
			// Component vectors are calculated, min for ensured upwards direction
			this.ball.body.velocity.y = Math.min(-80, (this.velocity * Math.sin(angle)));
			this.ball.body.velocity.x = (this.velocity * Math.cos(angle));
		}
	}

	// Returns the ball and paddle to the serving position
	resetBall() {
		this.tipText.visible = true;
		this.ball.body.velocity.set(0);
		this.ball.x = 300;
		this.ball.y = 770;
		this.ballInPlay = false;
	}

	// Called when ball falls out of the bottom
	ballLost() {
		this.combo = 0;
		this.comboText.text = "Combo 0";
		this.comboText.visible = false;
		this.lives--;
		this.livesText.text = "Lives " + this.lives;

		if (this.lives === 0) {
			this.gameOver();
		}
		else {
			this.resetBall();
		}
	}

	// Called when ball and paddle collide
	ballHitsPaddle (ball, paddle) {
		// Redeem combo as bonus points
		if (this.combo > 1) {
			this.score += this.combo * 100;
			this.scoreText.text = "Score " + this.score;
		}
		this.combo = 0;
		this.comboText.text = "Combo 0";
		this.comboText.visible = false;

		if (ball.x < paddle.x)
		{
			// Ball lands on the left
			let diff:number = paddle.x - ball.x;
			this.ball.body.velocity.x = (-10 * diff);
		}
		else if (ball.x >= paddle.x)
		{
			// Ball lands either on the right or dead center
			let diff:number = ball.x - paddle.x;
			this.ball.body.velocity.x = (10 * diff);
		}
		// recalculate y velocity
		this.ball.body.velocity.y = -Math.sqrt((this.velocity * this.velocity) - (this.ball.body.velocity.x * this.ball.body.velocity.x))
	}

	// Called when ball and brick collide
	ballHitsBrick (ball, brick) {
		brick.kill();
		this.score += 50;
		this.scoreText.text = 'Score ' + this.score;
		// add to combo count
		this.combo++;
		this.comboText.text = "Combo " + this.combo;
		if (this.combo > 1) {
			this.comboText.visible = true;
		}

		if (this.bricks.countLiving() === 0) {
			this.resetBall();
			this.velocity += 100;
			this.bricks.callAll('revive', null);
		}
	}

	// Called when player runs out of Lives
	gameOver() {
		// to add: wait for input then reset
		this.resetGame();
	}

	// Resets the current Game
	resetGame() {
		this.lives = 3;
		this.score = 0;
		this.livesText.text = 'Lives 3';
		this.scoreText.text = 'Score 0';

		this.bricks.callAll('revive', null);
		this.resetBall();
	}
}
